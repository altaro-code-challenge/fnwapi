﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FnwAPI.Models
{
    public class AirportCity
    {
        public int city_id { get; set; }
        public string iata_code { get; set; }
        public string city_name { get; set; }
    }
}