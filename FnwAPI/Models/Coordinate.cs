﻿namespace FnwAPI.Models
{
    public class Coordinate
    {
        // public int id { get; set; }
        public double lon { get; set; }
        public double lat { get; set; }
    }
}