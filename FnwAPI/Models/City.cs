﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FnwAPI.Models
{
    public class City
    {
        public int id { get; set; }
        public string city_name { get; set; }
        public string country_code { get; set; }
        public double city_longitude { get; set; }
        public double city_latitude { get; set; }
    }
}