﻿namespace FnwAPI.Models
{
    public class AirportDTO
    {
        public string city { get; set; }
        public string code { get; set; }
        public int? worldareacode { get; set; }
        public string country { get; set; }
        public double? longitude { get; set; }
        public double? latitude { get; set; }
        public string airportname { get; set; }
        public double? gmt { get; set; }
    }
}