﻿namespace FnwAPI.Models
{
    public class Airport
    {
        public int id { get; set; }
        public string airport_name { get; set; }
        public string city_name { get; set; }
        public string iata_code { get; set; }
        public string country_name { get; set; }
        public int? world_area_code { get; set; }
        public double? longitude { get; set; }
        public double? latitude { get; set; }
        public double? gmt { get; set; }
    }
}