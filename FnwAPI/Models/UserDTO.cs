﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FnwAPI.Models
{
    /// <summary>
    /// Contains only non-sensitive user information.
    /// </summary>
    public class UserData
    {
        public string username { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
    }

    /// <summary>
    /// User: Data Transfer Object
    /// User information retrieved from front-end while registering
    /// </summary>
    public class UserDTO : UserData
    {
        public string password { get; set; }
        public string confirm { get; set; }
    }
}