﻿namespace FnwAPI.Models
{

    /// <summary>
    /// Example:
    ///  {
    ///    "id": 707860,
    ///    "name": "Hurzuf",
    ///    "country": "UA",
    ///    "coord": {
    ///      "lon": 34.283333,
    ///      "lat": 44.549999
    ///    }
    ///  },
    /// </summary>
    public class CityDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public string country { get; set; }
        public Coordinate coord { get; set; }
    }
}
