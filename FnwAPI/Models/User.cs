﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FnwAPI.Models
{
    /// <summary>
    /// User information modeling the database structure, table users
    /// </summary>
    public class User
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int role_id { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
    }
}