﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FnwAPI.Helpers;
using MySql.Data.MySqlClient;

namespace FnwAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.EnableCors();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        public static MySqlConnection Connection()
        {
            string connectionString = Resources.MYSQL_CONNECTION_STRING;
           // string connectionString = "server=localhost;port=3306;database=fnw;username=fnw;password=eu5xq9WMUYtGWevQ;";
            MySqlConnection connection = new MySqlConnection(connectionString);
            return connection;
        }
    }
}
