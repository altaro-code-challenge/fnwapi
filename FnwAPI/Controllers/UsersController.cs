﻿using FnwAPI.Filters;
using FnwAPI.Models;
using FwnAPI.Helpers;
using FnwAPI.Helpers;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;

namespace FnwAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {

        // POST: api/users/register
        [AllowAnonymous]
        [HttpPost]
        [Route("api/users/register", Name = "RegisterNewUser")]
        public async Task<IHttpActionResult> PostAsync([FromBody] UserDTO userDTO)
        {
            // Check if data is not incomplete or malformed
            ResponseMessageResult userDTOValidationResult = ValidateUserDTO(userDTO);
            if (userDTOValidationResult.Response.StatusCode != HttpStatusCode.OK)
                return userDTOValidationResult;

            // Validate data: Check for duplicate username
            ResponseMessageResult checkDuplicateUsernameResult = await CheckDuplicateFieldAsync("username", userDTO.username);
            if (checkDuplicateUsernameResult.Response.StatusCode != HttpStatusCode.OK)
                return checkDuplicateUsernameResult;

            // Uncomment if we needed
            // // Validate data: Check for duplicate email
            // ResponseMessageResult checkDuplicateEmailResult = CheckDuplicateField("email", userDTO.email);
            // if (checkDuplicateEmailResult.Response.StatusCode != HttpStatusCode.OK)
            //     return checkDuplicateEmailResult;

            // Prepare the user data to be inserted in the database
            User user = MapUser(userDTO);

            // Insert the user in database, get the id if successful
            long insertedId = 0;
            ResponseMessageResult userInsertResult = await InsertUserAsync(user);
            if (userInsertResult.Response.StatusCode != HttpStatusCode.OK)
                return userInsertResult;

            insertedId = await userInsertResult.Response.Content.ReadAsAsync<long>();
            // Confirm that insert is successful
            IHttpActionResult confirmInsertResult = await ConfirmInsertAsync(insertedId);
            return confirmInsertResult;
        }

        #region register functions

        private ResponseMessageResult ValidateUserDTO(UserDTO userDTO)
        {
            try
            {
                if (userDTO == null)
                    throw ResponseError(HttpStatusCode.BadRequest, "Missing data");

                if (!ModelState.IsValid)
                    throw ResponseError(HttpStatusCode.BadRequest, "Missing or malformed data");

                if (userDTO.username == null
                    || userDTO.password == null
                    || userDTO.name == null
                    || userDTO.surname == null
                    || userDTO.email == null)
                    throw ResponseError(HttpStatusCode.BadRequest, "Missing or malformed data");

                // 'required fields' validation
                if (userDTO.username == ""
                    || userDTO.password == ""
                    || userDTO.name == ""
                    || userDTO.surname == ""
                    || userDTO.email == "")
                    throw ResponseError(HttpStatusCode.BadRequest, "Missing or malformed data");

                // validate email
                string pattern = @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
                Match m = Regex.Match(userDTO.email, pattern, RegexOptions.IgnoreCase);
                if (!m.Success) throw ResponseError(HttpStatusCode.BadRequest, "Email is not in correct format");

                // validate password confirmation
                if (userDTO.password != userDTO.confirm)
                    throw ResponseError(HttpStatusCode.BadRequest, "Password confirmation does not match");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }
        private async Task<ResponseMessageResult> CheckDuplicateFieldAsync(string fieldName, string value)
        {
            string query = "SELECT COUNT(*) FROM users " +
                            $" WHERE {fieldName} = '{value}';";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(Resources.MYSQL_CONNECTION_STRING))
                {
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        await connection.OpenAsync();
                        command.CommandText = query;
                        var usersFoundResult = await command.ExecuteScalarAsync();
                        long? usersFound = (long?)usersFoundResult;
                        if (usersFound == null) throw ResponseError(HttpStatusCode.InternalServerError, "Database query failed at check for duplicate users.");
                        if (usersFound > 0) throw ResponseError(HttpStatusCode.BadRequest, $"A user with that {fieldName} already exists.");

                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
                    }
                }
            }
            catch (MySqlException ex)
            {
                var logger = NLog.LogManager.GetCurrentClassLogger();
                logger.Error(ex);
                // throw 500
                throw ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
                //var logger = NLog.LogManager.GetCurrentClassLogger();
                //logger.Error(ex);
                //throw 500
                //throw ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private HttpResponseException ResponseError(HttpStatusCode statusCode, string message)
        {
            var errorResponseMessage = new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(message)
            };
            return new HttpResponseException(errorResponseMessage);
        }

        private async Task<IHttpActionResult> ConfirmInsertAsync(long insertedId)
        {
            string query = "SELECT username, name, surname, email FROM users " +
                           $" WHERE id = {insertedId};";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(Resources.MYSQL_CONNECTION_STRING))
                {
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        await connection.OpenAsync();
                        command.CommandText = query;
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                UserData userData = new UserData()
                                {
                                    username = (string)reader["username"],
                                    name = (string)reader["name"],
                                    surname = (string)reader["surname"],
                                    email = (string)reader["email"],
                                };

                                //return 201
                                return CreatedAtRoute("GetUserById",
                                                        new { id = insertedId },
                                                        userData);
                            }
                            else
                            {
                                //return 202
                                var logger = NLog.LogManager.GetCurrentClassLogger();
                                logger.Error("User was inserted, but could not be retrieved.");
                                return new ResponseMessageResult(
                                            Request.CreateErrorResponse(
                                                (HttpStatusCode)202,
                                                new HttpError("User was inserted, but could not be retrieved.")
                                            )
                                        );
                            }
                        }
                    }
                }
            }
            catch (MySqlException ex)
            {
                var logger = NLog.LogManager.GetCurrentClassLogger();
                logger.Error(ex);
                //return 202
                return new ResponseMessageResult(
                            Request.CreateErrorResponse(
                                (HttpStatusCode)202,
                                new HttpError("User was inserted, but could not be retrieved.")
                            )
                        );
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private User MapUser(UserDTO userDTO)
        {
            // Only regular users will be registering with this call, so RoleId will always be 2
            // Password will be stored securely with an encryption algorithm that uses dynamic salting
            User newUser = new User
            {
                name = userDTO.name,
                surname = userDTO.surname,
                email = userDTO.email,
                username = userDTO.username,
                role_id = 2,
                password = PasswordStorage.CreateHash(userDTO.password)
            };
            return newUser;
        }
        private async Task<ResponseMessageResult> InsertUserAsync(User user)
        {
            string query = "INSERT INTO users (username, password, role_id, name, surname, email)" +
                           " VALUES (" +
                          $"'{user.username}', '{user.password}', '{user.role_id}', " +
                          $"'{user.name}', '{user.surname}', '{user.email}');";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(Resources.MYSQL_CONNECTION_STRING))
                {
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        await connection.OpenAsync();
                        command.CommandText = query;
                        long insertUserResult = await command.ExecuteNonQueryAsync();
                        if (insertUserResult == 0) throw ResponseError(HttpStatusCode.InternalServerError, "Database insert failed.");

                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, command.LastInsertedId));
                    }
                }
            }
            catch (MySqlException ex)
            {
                var logger = NLog.LogManager.GetCurrentClassLogger();
                logger.Error(ex);
                // throw 500
                throw ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
                //var logger = NLog.LogManager.GetCurrentClassLogger();
                //logger.Error(ex);
                //throw 500
                //throw ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        #endregion
    }
}
