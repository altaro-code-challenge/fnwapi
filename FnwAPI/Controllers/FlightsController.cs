﻿using FnwAPI.Filters;
using FnwAPI.Models;
using FnwAPI.Helpers;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace FnwAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ContentController : ApiController
    {

        // GET: api/airports
        [JwtAuthentication]
        [HttpGet]
        [Route("api/airports")]
        public async Task<IEnumerable<Airport>> GetAllAirportsAsync()
        {
            return await GetAirportsFromDatabase("");
        }


        // GET: api/airports/?search...
        [JwtAuthentication]
        [HttpGet]
        [Route("api/airports")]
        public async Task<IEnumerable<Airport>> GetFilteredAirportsAsync([FromUri] string search)
        {
            return await GetAirportsFromDatabase(search);
        }

        // GET: api/airportcities/?departurecode=SPK&arrivalcode=CRL
        [JwtAuthentication]
        [HttpGet]
        [Route("api/airportcities")]
        public async Task<IEnumerable<AirportCity>> GetFilteredAirportCitiesAsync([FromUri] IATApair iataPair)
        {
            if (iataPair == null) throw new HttpResponseException(HttpStatusCode.BadRequest);
            try
            {
                return await GetAirportCitiesFromDatabase(iataPair);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }




        private async Task<IEnumerable<Airport>> GetAirportsFromDatabase(string search)
        {
            string query = ContructAirportsQuery(search);

            try
            {
                using (MySqlConnection connection = new MySqlConnection(Resources.MYSQL_CONNECTION_STRING))
                {
                    List<Airport> airports = new List<Airport>();
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        await connection.OpenAsync();
                        command.CommandText = query;
                        using (var reader = command.ExecuteReader())
                        {
                            while (await reader.ReadAsync())
                            {
                                Airport airport = new Airport()
                                {
                                    id = (int)reader["id"],
                                    airport_name = (string)reader["airport_name"],
                                    city_name = (string)reader["city_name"],
                                    iata_code = (string)reader["iata_code"],
                                    country_name = (string)reader["country_name"]
                                };
                                airports.Add(airport);
                            }
                            return airports;
                        }
                    }
                }
            }
            catch (MySqlException ex)
            {
                var logger = NLog.LogManager.GetCurrentClassLogger();
                logger.Error(ex);
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }
        private string ContructAirportsQuery(string search)
        {
            search = search.Trim().Replace("'", "''");
            string query = "SELECT id, airport_name, city_name, iata_code, country_name FROM airports WHERE 1 ";

            if (search != null || search != "") {
                query += $"AND airport_name LIKE '%{search}%' ";
                query += $"OR city_name LIKE '%{search}%' ";
                query += $"OR iata_code LIKE '%{search}%' ";
                query += $"OR country_name LIKE '%{search}%' ";
            }

            if (Int32.Parse(Resources.MAX_ITEMS_RETRIEVED) > 0)
            {
                query += $"LIMIT {Resources.MAX_ITEMS_RETRIEVED};";
            }
            return query;
        }

        private async Task<IEnumerable<AirportCity>> GetAirportCitiesFromDatabase(IATApair iataPair)
        {
            string query = ContructAirportCitiesQuery(iataPair);

            try
            {
                using (MySqlConnection connection = new MySqlConnection(Resources.MYSQL_CONNECTION_STRING))
                {
                    List<AirportCity> airportCities = new List<AirportCity>();
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        await connection.OpenAsync();
                        command.CommandText = query;
                        using (var reader = command.ExecuteReader())
                        {
                            while (await reader.ReadAsync())
                            {
                                AirportCity airportCity = new AirportCity()
                                {
                                    iata_code = (string)reader["iata_code"],
                                    city_id = (int)reader["city_id"],
                                    city_name = (string)reader["city_name"],
                                };
                                airportCities.Add(airportCity);
                            }
                            return airportCities.AsEnumerable<AirportCity>();
                        }
                    }
                }
            }
            catch (MySqlException ex)
            {
                var logger = NLog.LogManager.GetCurrentClassLogger();
                logger.Error(ex);
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }
        private string ContructAirportCitiesQuery(IATApair iataPair)
        {
            string query = "SELECT iata_code, city_name, city_id FROM airports_cities WHERE 1 ";

            if (iataPair != null)
            {
                query += $"AND iata_code = '{iataPair.departurecode}' ";
                query += $"OR iata_code = '{iataPair.arrivalcode}' ";
            }

            if (Int32.Parse(Resources.MAX_ITEMS_RETRIEVED) > 0)
            {
                query += $"LIMIT {Resources.MAX_ITEMS_RETRIEVED};";
            }
            return query;
        }


    }
}
