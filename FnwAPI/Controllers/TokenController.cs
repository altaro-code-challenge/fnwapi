﻿using FnwAPI.Filters;
using FnwAPI.Helpers;
using FnwAPI.Models;
using FwnAPI.Helpers;
using FnwAPI.Helpers;
using MySql.Data.MySqlClient;
using NLog.Fluent;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;

namespace FnwAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TokenController : ApiController
    {

        [JwtAuthentication]
        [HttpGet]
        [Route("api/check")]
        public IHttpActionResult CheckAuthenticated()
        {
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/token")]
        public async Task<string> Token([FromBody] UserAuthDTO userAuthDTO)
        {
            UserAuth userAuth = await RetrieveUser(userAuthDTO);
            if (PasswordStorage.VerifyPassword(userAuthDTO.password, userAuth.password))
            {
                //success, returns 200
                return JwtManager.GenerateToken(userAuth.username, userAuth.name);
            }
            else
            {
                //throw 401 with message 'wrong password'
                var logger = NLog.LogManager.GetCurrentClassLogger();
                logger.Info("Failed login: wrong password");
                var errorResponseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("Wrong password.")
                };
                throw new HttpResponseException(errorResponseMessage);
            }
            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }

        private async Task<UserAuth> RetrieveUser(UserAuthDTO userAuthDTO)
        {
            if (userAuthDTO.username == null || userAuthDTO.password == null)
            {
                //return 401
                var logger = NLog.LogManager.GetCurrentClassLogger();
                logger.Info("Failed login: invalid credentials");
                var errorResponseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("Invalid credentials")
                };
                throw new HttpResponseException(errorResponseMessage);
            }
        
            string query = "SELECT username, password, name FROM users " +
                                " WHERE username = '" + userAuthDTO.username + "';";

            try
            {
                using (MySqlConnection connection = new MySqlConnection(Resources.MYSQL_CONNECTION_STRING))
                {
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        await connection.OpenAsync();
                        command.CommandText = query;
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                UserAuth userAuth = new UserAuth()
                                {
                                    username = (string)reader["username"],
                                    password = (string)reader["password"],
                                    name = (string)reader["name"],
                                };

                                return userAuth;
                            }
                            else
                            {
                                //return 401
                                var logger = NLog.LogManager.GetCurrentClassLogger();
                                logger.Info("Failed login: invalid username");
                                var errorResponseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                                {
                                    Content = new StringContent("A user with that username does not exist.")
                                };
                                throw new HttpResponseException(errorResponseMessage);
                            }
                        }
                    }
                }
            }
            catch (MySqlException ex)
            {
                var logger = NLog.LogManager.GetCurrentClassLogger();
                logger.Error(ex);
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }
    }
}
